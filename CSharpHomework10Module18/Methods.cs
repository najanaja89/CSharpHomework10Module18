﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace CSharpHomework10Module18
{
    public class Methods
    {
        public void SaveBinaryFormat(List<Book> serialisableObjects, string fileName)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                binaryFormatter.Serialize(fileStream, serialisableObjects);
            }
        }

        public List<Book> OpenBinaryFormat(string fileName)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();

            using (FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                var deserialisableObjects = (List<Book>)binaryFormatter.Deserialize(fileStream);
                return deserialisableObjects;
            }
        }

        public void ShowBooks(List<Book> books)
        {
            foreach (var item in books)
            {
                Console.WriteLine("Name is " + item.Name);
                Console.WriteLine("Author is " + item.Author);
                Console.WriteLine("Year is " + item.Year);
                Console.WriteLine("Price is " + item.Price);
                Console.WriteLine("------------------------");
                Console.WriteLine();
            }
        }

        public void EditBook(List<Book> books)
        {
            Console.WriteLine("Press 1 to edit price of book by name");
            Console.WriteLine("Press 2 to add new book");
            string menu = Console.ReadLine();
            switch (menu)
            {
                case "1":
                    Console.WriteLine("Enter name of book");
                    string searchName = Console.ReadLine();
                    var result = books.Find(i => i.Name == searchName);
                    Console.WriteLine("Enter new price of book");
                    int searchPrice = Console.Read();
                    Console.ReadLine();
                    result.Price = searchPrice;
                    break;

                case "2":
                    Console.WriteLine("Enter name of book");
                    string name = Console.ReadLine();
                    Console.WriteLine("Enter author of book");
                    string author = Console.ReadLine();
                    Console.WriteLine("Enter year of book");
                    string year = Console.ReadLine();
                    Console.WriteLine("Enter price of book");
                    int price = Console.Read();
                    Console.ReadLine();
                    books.Add(new Book { Name = name, Author = author, Year = year, Price = price });
                    break;

                default:
                    break;
            }
        }
    }
}
