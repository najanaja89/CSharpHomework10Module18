﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;

namespace CSharpHomework10Module18
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Book> Books = new List<Book>
            {
                new Book { Name = "Hyperion", Author = "Dan Simmons", Price = 100, Year = "1989" },
                new Book { Name = "Metamorphosis", Author = "Franz Kafka", Price = 85, Year = "1915"},
                new Book { Name = "The Financier", Author = "Theodore Dreiser", Price = 110, Year = "1912"},
                new Book { Name = "The Gods Themselves", Author = "Isaac Asimov", Price = 76, Year = "1972"},
                new Book { Name = "Neuromancer", Author = "William Gibson", Price = 98, Year = "1984"},
            };
            Methods methods = new Methods();

            while (true)
            {
                Console.WriteLine("Press 1 to advanced features");
                Console.WriteLine("Press 2 to save data");
                Console.WriteLine("Press 3 to show saved data");
                Console.WriteLine("Press 4 to current list of books");
                Console.WriteLine("Press 0 to exit");
                string menu = Console.ReadLine();

                switch (menu)
                {
                    case "1":
                        methods.EditBook(Books);
                        break;

                    case "2":
                        methods.SaveBinaryFormat(Books, "Saved Books.dat");
                        break;

                    case "3":
                        var openedBooks = methods.OpenBinaryFormat("Saved Books.dat");
                        methods.ShowBooks(openedBooks);
                        break;

                    case "4":
                        methods.ShowBooks(Books);
                        break;

                    case "0":
                        Environment.Exit(0);
                        break;

                    default:
                        break;
                }

            }

            Console.ReadLine();
        }
    }
}
